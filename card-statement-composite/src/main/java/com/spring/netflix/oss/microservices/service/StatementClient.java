package com.spring.netflix.oss.microservices.service;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "statement-service")
public interface StatementClient extends StatementService{

}